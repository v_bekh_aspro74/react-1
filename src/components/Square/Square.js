import React from 'react';
import './Square.css';
import {GameModeContext} from '../../context/GameMode/GameMode';

class Square extends React.Component {
    render() {
        let squareColor = this.context.calculateSquareColor(this.props.info.id);

        let size = this.props.preview ? this.context.squareSize / 4 : this.context.squareSize;
        let squareStyles = {
            height: size,
            width: size,
            padding:   this.context.squaresPadding + '%',        
        }

        return (
            <div 
                className={
                    'square' +
                    (this.props.preview ? ' square--preview' : '') + 
                    (this.props.winner ? ' square--winner' : '') + 
                    (this.props.lastClicked && !this.props.winner ? ' square--last-clicked' : '') +
                    ' square--color-' + squareColor +
                    (this.props.availableForStep ? ' '+this.context.availableClass : '')

                }
                onClick={this.props.onClick}
                style={squareStyles}
            >
                {this.getImage()}
            </div>
        );
    }   

    getImage() {
        let image = null;
        if(this.props.info.value) {
            let imageKey = 'default';
            if(this.props.info.image) {
            imageKey = this.props.info.image;
            }
            image = this.context.players.list[this.props.info.value].image[imageKey];
        }
        return image;
    }
}

Square.contextType = GameModeContext;

export default Square;
import React from 'react';
import './Select.css';
import { ReactComponent as Arrow } from './Select__arrow-down.svg';

class Select extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.default,
            dropdownVisible: false,
        };
    }

    render() {
        return (
            <div className="select">
                {this.showLabel(this.props.label)}
                <div 
                    className={'select__title' + (this.state.dropdownVisible ? ' active' : '')}
                    onClick={() => this.setState({dropdownVisible: !this.state.dropdownVisible})}
                >
                <div className='select__sub-title'>{this.props.options[this.state.value].name}{<Arrow />}</div>
                    <div className='select__dropdown'>
                        {this.showDropdown()}
                    </div>
                </div>
                {this.showHiddenSelect()}
            </div>
        );
    }

    selectClick(optionKey) {
        this.setState({value: optionKey});
        this.props.onClick(optionKey);
    }

    showDropdown() {
        let options = [];
        let counter = 1;
        let optionsCount = Object.keys(this.props.options).length;
        for(let optionKey in this.props.options) {
            let option = this.props.options[optionKey];
            options.push(
            <div
                key={option.id}
                className={'select__dropdown-item' + (counter === optionsCount ? ' last' : (counter === 1 ? ' first' : ''))}
                onClick={() => this.selectClick(optionKey)}
            >
                {option.name}
            </div>);
            counter++;
        }
        return options;
    }

    showHiddenSelect() {
        let options = [];
        for(let optionKey in this.props.options) {
            let option = this.props.options[optionKey];
            options.push(<option key={option.id} value={optionKey}>{option.name}</option>);
        }
        return (
            <select className='select__input' value={this.state.value} name={this.props.name} readOnly>
                {options}
            </select>
        );
    }

    showLabel(label) {
        let result = '';
        if(label) {
            result = <label className='select__label'>{label}</label>;
        }
        return result;
    }

}

export default Select;
import React from 'react';
import './StepsList.css';
import Board from '../Board/Board';

function StepsList (props) {
    let result = props.history.map((step, move) => {
        return (
            <div className={'steps-list__step' + (props.currentMove === move ? ' steps-list__step--active' : '')} key={move}>
                <div className='steps-list__button' onClick={() => props.onClick(move)}>
                    <Board
                        currentStep={step}
                        preview={true}
                    />
                </div>
            </div>
        );
    });

    if(props.reverse) {
        result = result.reverse();
    }

    return (
        <div className="steps-list">
            {result}
        </div>
    );
}

export default StepsList;
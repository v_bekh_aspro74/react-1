import React from 'react';
import './Board.css';
import Square from '../Square/Square';
import {GameModeContext} from '../../context/GameMode/GameMode';

class Board extends React.Component {
    handleClick(key) {
      if(!this.props.disabled && this.checkAvailable(key).result) {
        this.props.onClick(key);
      }
    }

    renderSquare(key) {
      let winner = false;
      if( this.winner.winnerSquares ) {
        winner = this.winner.winnerSquares.indexOf(key) !== -1;
      }

      let availableForStep = false;
      if(this.availableSquares) {
        availableForStep = this.availableSquares.indexOf(key) !== -1;
      }
      
      let currentSquare = this.props.currentStep.squares[key];

      return (
        <Square
          key={key}
          lastClicked={this.props.currentStep.lastClicked === key}
          info={currentSquare}
          winner={winner}
          preview={this.props.preview}
          availableForStep={availableForStep}
          currentPlayer={this.props.currentStep.currentPlayer.id}
          onClick={() => this.handleClick(key) }
        />
      );
    }

    renderRow(size, row) {
      let result = [];
      for(let i = row*size;i < row*size + size;i++) {
        result.push( this.renderSquare(i) );
      }
      return result;
    }

    renderBoard(size) {
      let sizeArray = Array(size).fill(null);
      this.availableSquares = [];

      if(!this.props.preview) {
        let availableChop = [],
            availableEmpty = [];
        for(let i = 0;i < size*size;i++) {
          let available = this.checkAvailable(i);
          if(available.result) {
            if(available.chop) {
              availableChop.push(i);
            } else {
              availableEmpty.push(i);
            }
          }
        }
        if(availableChop.length) {
          this.availableSquares = availableChop;
        } else {
          this.availableSquares = availableEmpty;
        }
      }

      const board = sizeArray.map((el, i) => {
        return(
          <div key={i} className="board__row">
            {this.renderRow(size, i)}
          </div>
        )
      });

      return board;
    }

    checkAvailable = (key) => {
      if(this.props.preview) {
        return false;
      }

      let availableInfo = {
        squares: this.props.currentStep.squares,
        squareIndex: key,
        currentPlayer: this.props.currentStep.currentPlayer,
        gameIsEnd: Boolean(this.winner),
        availableSquares: this.props.availableSquares,
      }

      let result = this.context.checkAvailable(availableInfo);
      return result;
    }

    render() {
      this.winner = this.context.calculateWinner(this.props.currentStep.squares);
      if(!this.winner.message) {
        this.winner = false;
      }
      
      return (
        <div className="board">
          {this.renderBoard(this.context.boardSize)}
        </div>
      );
    }
}

Board.contextType = GameModeContext;

export default Board;
import React from 'react';
import './Switcher.css';

class Switcher extends React.Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div 
                className="switcher"
            >
                {this.showLabel(this.props.label)}
                <div 
                    className={'switcher__track' + (this.props.active ? ' active' : '')}
                    onClick={
                        () => {
                            this.props.onClick(!this.props.active);
                        }
                    }
                >
                    <div className='switcher__boll'></div>
                </div>
                <input type='checkbox' className='switcher__checkbox' checked={this.props.active ? ' checked' : ''} name={this.props.name} readOnly />
            </div>
        );
    }

    showLabel(label) {
        let result = '';
        if(label) {
            result = <label className='switcher__label'>{label}</label>;
        }
        return result;
    }

}

export default Switcher;
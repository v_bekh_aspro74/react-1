import React from 'react';
import './Game.css';
import {GameModeContext, GameMode} from '../../context/GameMode/GameMode';
import Board from '../Board/Board';
import StepsList from '../StepsList/StepsList';
import Switcher from '../Switcher/Switcher';
import Select from '../Select/Select';

class Game extends React.Component {
    constructor(props) {
      super(props);

      this.gameModDefaultName = 'chess';
      let gameModeDefault = GameMode[this.gameModDefaultName];
      let playerDefault = gameModeDefault.players.list[ gameModeDefault.players.default ];

      this.state = {
        gameMode: gameModeDefault,
        history: [{
          squares: gameModeDefault.squares,
          currentPlayer: playerDefault,
        }],
        stepNumber: 0,
        sortReverse: false,
        availableSquares: null,
        userSquare: null,
      };
    }

    render() {
      const history = this.state.history;
      const current = history[ this.state.stepNumber ];
      const winner = this.state.gameMode.calculateWinner(current.squares);

      let status;
      if (winner) {
        status = winner.message;
      } else {
        status = 'Следующий ход: ' + (current.currentPlayer.name);
      }

      return (
        <div className={"game " + this.state.gameMode.id}>
          <GameModeContext.Provider value={this.state.gameMode}>
            <div className="game__board">
              <Board
                currentStep={current}
                availableSquares={this.state.availableSquares}
                onClick={(i) => this.handleClick(i)}
              />
            </div>
            <div className="game__info">
              <Select
                label='Выберите игру'
                onClick={game => this.selectGameClick(game)}
                name='choose_game'
                options={GameMode}
                default={this.gameModDefaultName}
              /> 
              <div className="game__status">{status}</div>
              <Switcher
                label='Ходы по убыванию'
                active={this.state.sortReverse}
                onClick={active => this.sortClick(active)}
                name='sort_desk'
              /> 
              <StepsList 
                history={history}
                currentMove={this.state.stepNumber}
                onClick={move => this.jumpTo(move)}
                reverse={this.state.sortReverse}
              />
            </div>
          </GameModeContext.Provider>
        </div>
      );
    }

    handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[this.state.stepNumber];
      let squares = current.squares.slice();
      let winner = this.state.gameMode.calculateWinner(squares);
      let currentPlayer = current.currentPlayer;

      if(winner || this.state.gameMode.stopAction(squares, i) ) {
        return;
      }
      
      let availableSquares = {
        steps: []
      };
      if(this.state.userSquare !== i) {
        availableSquares = this.state.gameMode.getAvailableSteps(squares, i, currentPlayer);
      }

      if( availableSquares ) {
        if(availableSquares.steps.length) {
          this.setState({
            availableSquares: availableSquares,
            userSquare: i,
          });
        } else {
          this.setState({
            availableSquares: null,
            userSquare: null,
          });
        }
        
        return;
      }

      let newStepInfo = this.state.gameMode.generateNewStep(squares, i, currentPlayer, this.state.userSquare);
      squares = newStepInfo.squares;

      if(newStepInfo.chopIndex !== false) {
        availableSquares = this.state.gameMode.getAvailableSteps(squares, i, currentPlayer, true);
      }

      let userSquare = i;
      if( !availableSquares || !availableSquares.steps.length ) {
        availableSquares = null;
        userSquare = null;
        currentPlayer = this.state.gameMode.players.changePlayer( currentPlayer );
      }

      this.setState({
        history: history.concat([{
          squares: squares,
          lastClicked: i,
          currentPlayer: currentPlayer,
        }]),
        stepNumber: history.length,
        availableSquares: availableSquares,
        userSquare: userSquare,
      });
      
    }

    jumpTo(step) {
      if(this.state.stepNumber === step)
        return false;

      this.setState({
        stepNumber: step,
      })
    }

    sortClick(active) {
      this.setState({
        sortReverse: active
      });
    }

    selectGameClick(game) {
      let currentGame = GameMode[game];
      let playerDefault = currentGame.players.list[ currentGame.players.default ];

      this.setState({
        gameMode: currentGame,
        history: [{
          squares: currentGame.squares,
          currentPlayer: playerDefault,
        }],
        stepNumber: 0,
      });
    }
}

export default Game;
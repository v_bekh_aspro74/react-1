import React from 'react';
import { ReactComponent as CrossImage } from './images/tick-tack-toe/CrossImage.svg';
import { ReactComponent as ToeImage } from './images/tick-tack-toe/ToeImage.svg';

import { ReactComponent as WhiteCheckers } from './images/checkers/WhiteCheckers.svg';
import { ReactComponent as BlackCheckers } from './images/checkers/BlackCheckers.svg';
import { ReactComponent as WhiteKing } from './images/checkers/WhiteKing.svg';
import { ReactComponent as BlackKing } from './images/checkers/BlackKing.svg';

import { ReactComponent as WhiteChessPawn } from './images/chess/WhiteChessPawn.svg';
import { ReactComponent as WhiteChessRook } from './images/chess/WhiteChessRook.svg';
import { ReactComponent as WhiteChessKnight } from './images/chess/WhiteChessKnight.svg';
import { ReactComponent as WhiteChessBishop } from './images/chess/WhiteChessBishop.svg';
import { ReactComponent as WhiteChessQueen } from './images/chess/WhiteChessQueen.svg';
import { ReactComponent as WhiteChessKing } from './images/chess/WhiteChessKing.svg';
import { ReactComponent as BlackChessPawn } from './images/chess/BlackChessPawn.svg';
import { ReactComponent as BlackChessRook } from './images/chess/BlackChessRook.svg';
import { ReactComponent as BlackChessKnight } from './images/chess/BlackChessKnight.svg';
import { ReactComponent as BlackChessBishop } from './images/chess/BlackChessBishop.svg';
import { ReactComponent as BlackChessQueen } from './images/chess/BlackChessQueen.svg';
import { ReactComponent as BlackChessKing } from './images/chess/BlackChessKing.svg';

export const GameMode = {
    'tick-tack-toe': {
        id: 'tick-tack-toe',
        name: 'Крестики-нолики',
        boardSize: 3,
        coloredBoard: false,
        squareSize: 140,
        availableClass: 'square--available-hover',
        squaresPadding: 3,
        players: {
            default: 'X',
            list: {
                'X': {
                    name: 'Крестики',
                    id: 'X',
                    image: {
                        default: <CrossImage />
                    },
                },
                'O': {
                    name: 'Нолики',
                    id: 'O',
                    image: {
                        default: <ToeImage />
                    },
                },
            },
            changePlayer: function(currentPlayer) {
                let nextPlayer = currentPlayer.id === 'X' ? this.list['O'] : this.list['X'];
                return nextPlayer;
            },
        },
        calculateWinner: function(currentSquares) {
            let result = {};
            const lines = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6],
            ];
            for (let i = 0; i < lines.length; i++) {
                const [a, b, c] = lines[i];
                if (currentSquares[a].value && currentSquares[a].value === currentSquares[b].value && currentSquares[a].value === currentSquares[c].value) {
                    result.winnerSquares = lines[i];
                    result.message = 'Выиграли - ' + this.players.list[ currentSquares[a].value ].name;
                    return result;
                }
            }
            return false;
        },
        stopAction: function(currentSquares, clickedSquare) {
            return Boolean(currentSquares[clickedSquare].value);
        },
        calculateSquareColor(i) {
            return '';
        },
        checkAvailable(availableInfo) {
            let currentValue = availableInfo.squares[availableInfo.squareIndex].value;
            let result = {};
            if(availableInfo.gameIsEnd) {
                result.result = false;
            } else {
                result.result = currentValue === null;
            }
            return result;
        },
        fillSquares(boardSize) {
            let arSquares = Array(boardSize*boardSize).fill(null);
        
            arSquares = arSquares.map(
                (el, i) => {   
                    let squareInfo = {
                        id: i,
                        value: null,
                        // image
                    };        
                    
                    return squareInfo;
                }
            );
        
            return arSquares;
        },
        generateNewStep(currentSquares, squareIndex, currentPlayer) {
            const clickedSquare = Object.assign({}, currentSquares[squareIndex]);
            clickedSquare.value = currentPlayer.id;
            currentSquares[squareIndex] = clickedSquare;
            return {
                squares: currentSquares
            }
        },
        getAvailableSteps(squares, i, currentPlayer) {
            return false;
        }
    },
    'checkers': {
        id: 'checkers',
        name: 'Шашки',
        boardSize: 8,
        coloredBoard: true,
        squareSize: 80,
        availableClass: 'square--available',
        squaresPadding: 3,
        players: {
            default: 'white',
            list: {
                'white': {
                    name: 'Белые',
                    id: 'white', 
                    image: {
                        default: <WhiteCheckers />,
                        king: <WhiteKing />
                    },
                },
                'black': {
                    name: 'Черные',
                    id: 'black', 
                    image: {
                        default: <BlackCheckers />,
                        king: <BlackKing />
                    },
                },
            },
            changePlayer: function(currentPlayer) {
                let nextPlayer = currentPlayer.id === 'white' ? this.list['black'] : this.list['white'];
                return nextPlayer;
            },
        },
        calculateWinner: function(currentSquares) {
            let squares = [];
            let result = false;

            for (const key in currentSquares) {
                if(currentSquares[key].value) {
                    squares.push( currentSquares[key].value );
                }
            }
            let isWhiteExist = squares.indexOf( 'white' ) !== -1;
            let isBlackExist = squares.indexOf( 'black' ) !== -1;
            
            if(!isWhiteExist || !isBlackExist){
                result = {
                    message: 'Выиграли - ' + this.players.list[ squares[0] ].name,
                    winner: squares[0],
                };
            }

            return result;
        },
        stopAction: function(currentSquares, clickedSquare) {
            return false;
        },
        calculateSquareColor(i) {
            let isEven = i % 2 === 0;
            let rowNumber = Math.trunc(i / 8 + 1);
            let isEvenRow = rowNumber % 2 === 0;
            
            if( (isEvenRow && isEven) || (!isEvenRow && !isEven) ) {
                return 'black';
            } else {
                return 'white';
            }
        },
        checkAvailable(availableInfo) {
            let result = {};
            if( availableInfo.availableSquares ) {
                if(availableInfo.availableSquares.steps.indexOf(availableInfo.squareIndex) !== -1) {          
                    result = {
                        result: true,
                    };
                }
            } else {        
                let arAvaibleForStep = this.getAvailableSteps(availableInfo.squares, availableInfo.squareIndex, availableInfo.currentPlayer);
                if(arAvaibleForStep && arAvaibleForStep.steps.length > 1) {      
                    result = {
                        result: true,
                        chop: arAvaibleForStep.chop,
                    };
                }
            }
            return result;
        },
        fillSquares(boardSize) {
            let arSquares = Array(boardSize*boardSize).fill(null);
        
            arSquares = arSquares.map(
                (el, i) => {   
                    let squareInfo = {
                        id: i,
                        value: null,
                        // image
                    };
        
                    if(i < boardSize * 3 || i >= boardSize * 5) {
                        let isEven = i % 2 === 0;
                        let rowNumber = Math.trunc(i / boardSize + 1);
                        let isEvenRow = rowNumber % 2 === 0;
                        let color = i < boardSize * 3 ? 'black' : 'white';
                        
                        if( (isEvenRow && isEven) || (!isEvenRow && !isEven) ) {
                            squareInfo.value = color;
                        }
                    }            
                    
                    /*if(i === 14 ){
                         squareInfo.value = 'white';
                    } else if(i === 55  ){
                        squareInfo.value = 'black';
                    } else if(i === 7 || i === 21 || i === 62 || i === 46  ){
                         squareInfo.value = null;
                    }*/
                    
                    return squareInfo;
                }
            );
        
            return arSquares;
        },
        generateNewStep(currentSquares, squareIndex, currentPlayer, userSquareIndex) {
            const clickedSquare = Object.assign({}, currentSquares[userSquareIndex]);

            let chopIndex = false;
            let _this = this;
            let rowNumber = Math.trunc(squareIndex / _this.boardSize);
            

            chopIndex = this.getChopIndex(userSquareIndex, squareIndex, currentSquares);
            if(chopIndex !== false){
                currentSquares[chopIndex] = {
                    id:chopIndex,
                    value: null,
                };
            }

            if( ( clickedSquare.value === 'white' && rowNumber === 0 ) || ( clickedSquare.value === 'black' && rowNumber === ( _this.boardSize -1 ) ) ){     
                clickedSquare.image = 'king';
            }

            currentSquares[squareIndex] = clickedSquare;
            currentSquares[userSquareIndex] = {
                id: userSquareIndex,
                value: null,
            };

            let result = {
                squares: currentSquares,
                chopIndex: chopIndex,
            }

            return result;
        },
        getAvailableSteps(currentSquares, squareIndex, currentPlayer, onlyChop) {
            let currentPlayerId = currentPlayer.id;
            let isAvailable = currentSquares[squareIndex].value === currentPlayerId;
            
            if( !isAvailable ){
                return false;
            }
            
            let result = {
                    chop: false,
                    steps: [],
                },
                resultEmpty = [],
                resultChop = [];

            if( !onlyChop ) {
                result.steps.push(squareIndex);
            }

            let squaresIndexes = [
                {row: -1, column: -1, playerId: 'white'},
                {row: -1, column:  1, playerId: 'white'},
                {row:  1, column: -1, playerId: 'black'},
                {row:  1, column:  1, playerId: 'black'}
            ];
            let _this = this;

            squaresIndexes.forEach(element => {
                let diagonalResult = _this.checkDiagonal(squareIndex, currentSquares, element, onlyChop, currentPlayerId);
                if(diagonalResult) {
                    if(diagonalResult.chop.length) {
                        resultChop = resultChop.concat(diagonalResult.chop);
                    } else if (diagonalResult.empty.length) {
                        resultEmpty = resultEmpty.concat(diagonalResult.empty);
                    }
                }
            });
            
            if(resultChop.length) {
                result.steps = result.steps.concat(resultChop);
                result.chop = true;
            } else {
                result.steps = result.steps.concat(resultEmpty);
            }
            return result;     
        },
        checkBoardSize(rowNumber, columnNumber, element, boardSize, chopIndex){
            let result = true;
            if(rowNumber + element.row*chopIndex < 0 || rowNumber + element.row*chopIndex > boardSize - 1){
                result = false;
            } else if(columnNumber + element.column*chopIndex < 0 || columnNumber + element.column*chopIndex > boardSize - 1){
                result = false;
            }

            return result;
        },
        getChopIndex(indexFrom, indexTo, currentSquares){  
            let _this = this;

            let rowNumberFrom = Math.trunc(indexFrom / _this.boardSize);
            let columnNumberFrom = indexFrom % _this.boardSize;
            let rowNumberTo = Math.trunc(indexTo / _this.boardSize);
            let columnNumberTo = indexTo % _this.boardSize;
            let growRowIndex = rowNumberTo > rowNumberFrom ? 1 : -1;
            let growColumnIndex = columnNumberTo > columnNumberFrom ? 1 : -1;

            let playerId = currentSquares[indexFrom].value;
            let result = false;

            while(rowNumberFrom !== rowNumberTo) {
                rowNumberFrom += growRowIndex;
                columnNumberFrom += growColumnIndex;

                let indexCurrent = rowNumberFrom * _this.boardSize + columnNumberFrom;
                let squareCurrent = currentSquares[indexCurrent];
                if( squareCurrent.value && squareCurrent.value !== playerId ) {
                    result = indexCurrent;
                    break;
                }
            }

            return result;
            
        },
        checkDiagonal(currentIndex, currentSquares, element, onlyChop, currentPlayerId) {
            let _this = this;
            let rowNumber = Math.trunc(currentIndex / _this.boardSize);
            let columnNumber = currentIndex % _this.boardSize;
            let isKing = currentSquares[currentIndex].image === 'king';
            let maxDiag = isKing ? _this.boardSize : 2;
            let result = {
                chop: [],
                empty: [],
                result: false,
            };

            for(let diag = 1; diag < maxDiag; diag++){
                let index = (rowNumber + element.row*diag) * _this.boardSize + (columnNumber + element.column*diag);
                if(_this.checkBoardSize(rowNumber, columnNumber, element, _this.boardSize, diag)){
                    if(currentSquares[index].value === null){
                        if(result.chop.length) {
                            result.chop.push(index);
                        } else {
                            if( !onlyChop ) {
                                if(isKing || element.playerId === currentPlayerId) {
                                    result.empty.push(index);
                                    result.result = true;
                                }
                            }
                        }
                    } else if(currentSquares[index].value !== currentPlayerId) {
                        let indexChop = ( rowNumber + element.row*(diag + 1) ) * _this.boardSize + (columnNumber + element.column*(diag + 1));
                        if(_this.checkBoardSize(rowNumber, columnNumber, element, _this.boardSize, (diag + 1) )){
                            if(currentSquares[indexChop].value === null && !result.chop.length){
                                result.chop.push(indexChop);
                                result.result = true;
                            } else {
                                break;
                            }                    
                        }
                    } else {
                        break;
                    }       
                }
            }

            if(result.result) {
                return result;
            } else {
                return false;
            }
        }
    },
    'chess': {
        id: 'chess',
        name: 'Шахматы',
        boardSize: 8,
        coloredBoard: true,
        squareSize: 80,
        availableClass: 'square--available',
        squaresPadding: 1,
        players: {
            default: 'white',
            list: {
                'white': {
                    name: 'Белые',
                    id: 'white', 
                    image: {
                        default: <WhiteChessPawn />,
                        rook: <WhiteChessRook />,
                        knight: <WhiteChessKnight />,
                        bishop: <WhiteChessBishop />,
                        queen: <WhiteChessQueen />,
                        king: <WhiteChessKing />,
                    },
                },
                'black': {
                    name: 'Черные',
                    id: 'black', 
                    image: {
                        default: <BlackChessPawn />,
                        rook: <BlackChessRook />,
                        knight: <BlackChessKnight />,
                        bishop: <BlackChessBishop />,
                        queen: <BlackChessQueen />,
                        king: <BlackChessKing />,
                    },
                },
            },
            changePlayer: function(currentPlayer) {
                let nextPlayer = currentPlayer.id === 'white' ? this.list['black'] : this.list['white'];
                return nextPlayer;
            },
        },
        calculateWinner: function(currentSquares) {
            let squares = [];
            let result = false;

            for (const key in currentSquares) {
                if(currentSquares[key].value) {
                    squares.push( currentSquares[key].value );
                }
            }
            let isWhiteExist = squares.indexOf( 'white' ) !== -1;
            let isBlackExist = squares.indexOf( 'black' ) !== -1;
            
            if(!isWhiteExist || !isBlackExist){
                result = {
                    message: 'Выиграли - ' + this.players.list[ squares[0] ].name,
                    winner: squares[0],
                };
            }

            return result;
        },
        stopAction: function(currentSquares, clickedSquare) {
            return false;
        },
        calculateSquareColor(i) {
            let isEven = i % 2 === 0;
            let rowNumber = Math.trunc(i / 8 + 1);
            let isEvenRow = rowNumber % 2 === 0;
            
            if( (isEvenRow && isEven) || (!isEvenRow && !isEven) ) {
                return 'black';
            } else {
                return 'white';
            }
        },
        checkAvailable(availableInfo) {
            let result = {};
            if( availableInfo.availableSquares ) {
                if(availableInfo.availableSquares.steps.indexOf(availableInfo.squareIndex) !== -1) {          
                    result = {
                        result: true,
                    };
                }
            } else {
                let arAvailableForStep = this.getAvailableSteps(availableInfo.squares, availableInfo.squareIndex, availableInfo.currentPlayer);
                if(arAvailableForStep && arAvailableForStep.steps.length > 1) {      
                    result = {
                        result: true,
                        chop: arAvailableForStep.chop,
                    };
                }
            }
            return result;
        },
        fillSquares(boardSize) {
            let arSquares = Array(boardSize*boardSize).fill(null);
        
            arSquares = arSquares.map(
                (el, i) => {   
                    let squareInfo = {
                        id: i,
                        value: null,
                        // image
                    };
        
                    if(i < boardSize * 2 || i >= boardSize * 6) {
                        let rowNumber = Math.trunc(i / boardSize + 1);
                        let columnNumber = i % boardSize;
                        let color = i < boardSize * 2 ? 'black' : 'white';

                        squareInfo.value = color;

                        if( (rowNumber === 1) || (rowNumber === boardSize) ) {
                            switch (columnNumber) {
                                case 0:
                                case 7:
                                    squareInfo.image = 'rook';
                                    break;
                                case 1:
                                case 6:
                                    squareInfo.image = 'knight';
                                    break;
                                case 2:
                                case 5:
                                    squareInfo.image = 'bishop';
                                    break;
                                case 3:
                                    squareInfo.image = 'queen';
                                    break;
                                case 4:
                                    squareInfo.image = 'king';
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    
                    return squareInfo;
                }
            );
        
            return arSquares;
        },
        generateNewStep(currentSquares, squareIndex, currentPlayer, userSquareIndex) {
            const clickedSquare = Object.assign({}, currentSquares[userSquareIndex]);
            clickedSquare.id = squareIndex;

            let chopIndex = false;
            
            currentSquares[squareIndex] = clickedSquare;
            currentSquares[userSquareIndex] = {
                id: userSquareIndex,
                value: null,
            };

            let result = {
                squares: currentSquares,
                chopIndex: chopIndex,
            }

            return result;
        },
        getAvailableSteps(currentSquares, squareIndex, currentPlayer, onlyChop, enemySteps, enemyChecked) { // onlyChop for compatible
            let currentPlayerId = currentPlayer.id;
            let currentSquare = currentSquares[squareIndex];
            let isAvailable = enemySteps ? currentSquare.value : currentSquare.value === currentPlayerId;
            
            if( !isAvailable ){
                return false;
            }
            
            let result = {
                steps: [],
            };

            // если вражеская фигура это ладья, слон или ферзь и она может срубить текущую клетку
            // то нужно убрать текущую фигуру с доски и получить доступные ходы для текущей фигуры.
            // через отдельную функцию проверить, есть ли среди этих ходов король
            // то мы рассчитываем ходы для нашей фигуры и сотавляем только те, которые совпадают с вражеской

            switch (currentSquare.image) {
                case 'queen':
                    result.steps = this.getStepsQueen(currentSquares, squareIndex, enemySteps);
                    break;
                case 'bishop':
                    result.steps = this.getStepsBishop(currentSquares, squareIndex, enemySteps);
                    break;
                case 'knight':
                    result.steps = this.getStepsKnight(currentSquares, squareIndex, enemySteps);
                    break;
                case 'rook':
                    result.steps = this.getStepsRook(currentSquares, squareIndex, enemySteps);
                    break;
                case 'king':
                    result.steps = this.getStepsKing(currentSquares, squareIndex, currentPlayer, enemyChecked);
                    break;
                default:
                    result.steps = this.getStepsPawn(currentSquares, squareIndex, enemySteps);                    
                    break;
            }

            //push current position
            result.steps.push(squareIndex);
            
            return result;     
        },
        getStepsRook(currentSquares, squareIndex, enemySteps) {
            let result = [];
            let resultTmp = [];
            let position = this.getRowColumn(squareIndex);

            for(let i = 1;i <= this.boardSize;i++) {
                let checkIndex = this.getIndex(i, position.column);

                if(i > position.row) {
                    if(currentSquares[checkIndex].value === null) {
                        result.push(checkIndex);
                    } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                        result.push(checkIndex);
                        break;
                    } else {
                        if(enemySteps) {
                            result.push(checkIndex);
                        }
                        break;
                    }
                } else if(i < position.row) {
                    if(currentSquares[checkIndex].value === null) {
                        resultTmp.push(checkIndex);
                    } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                        resultTmp = [];
                        resultTmp.push(checkIndex);
                    } else {
                        resultTmp = [];
                    }
                } else {
                    continue;
                }
            }
            if(resultTmp.length) {
                result = result.concat(resultTmp);
            }

            for(let i = 1;i <= this.boardSize;i++) {
                let checkIndex = this.getIndex(position.row, i);

                if(i > position.column) {
                    if(currentSquares[checkIndex].value === null) {
                        result.push(checkIndex);
                    } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                        result.push(checkIndex);
                        break;
                    } else {
                        break;
                    }
                } else if(i < position.column) {
                    if(currentSquares[checkIndex].value === null) {
                        resultTmp.push(checkIndex);
                    } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                        resultTmp = [];
                        resultTmp.push(checkIndex);
                    } else {
                        resultTmp = [];
                    }
                } else {
                    continue;
                }
            }
            if(resultTmp.length) {
                result = result.concat(resultTmp);
            }

            return result;
        },
        getStepsKnight(currentSquares, squareIndex, enemySteps) {
            let result = [];
            let position = this.getRowColumn(squareIndex);
            let needSquares = [
                {row: -2, column: -1,},
                {row: -2, column: +1,},
                {row: -1, column: +2,},
                {row: +1, column: +2,},
                {row: +2, column: -1,},
                {row: +2, column: +1,},
                {row: +1, column: -2,},
                {row: -1, column: -2,},
            ];
            
            needSquares.forEach((offset) => {
                let checkIndex = this.getIndex(position.row + offset.row, position.column + offset.column);
                if(checkIndex >= 0 && checkIndex < this.boardSize*this.boardSize && checkIndex !== false) {
                    if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value || enemySteps) {
                        result.push(checkIndex);
                    }
                }
            });

            return result;
        },
        getStepsBishop(currentSquares, squareIndex, enemySteps) {
            let result = [];
            let position = this.getRowColumn(squareIndex);

            let needDirections = [
                {row: -1, column: -1,},
                {row: +1, column: +1,},
                {row: +1, column: -1,},
                {row: -1, column: +1,},
            ];
            
            needDirections.forEach((offset) => {
                for(let i = 1;i < this.boardSize;i++) {
                    let newRow = position.row + offset.row*i;
                    let newColumn = position.column + offset.column*i;

                    if(newRow > 0 && newRow <= this.boardSize && newColumn > 0 && newColumn <= this.boardSize) {
                        let checkIndex = this.getIndex(newRow, newColumn);
                        if(checkIndex >= 0 && checkIndex < this.boardSize*this.boardSize) {
                            if(currentSquares[checkIndex].value === null) {
                                result.push(checkIndex);
                            } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                                result.push(checkIndex);
                                break;
                            } else {
                                if(enemySteps) {
                                    result.push(checkIndex);
                                }
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }
            });


            return result;
        },
        getStepsQueen(currentSquares, squareIndex, enemySteps) {
            let result = this.getStepsRook(currentSquares, squareIndex, enemySteps);
            result = result.concat( this.getStepsBishop(currentSquares, squareIndex, enemySteps) );            

            return result;
        },
        getStepsKing(currentSquares, squareIndex, currentPlayer, enemyChecked) {
            let result = [];
            let position = this.getRowColumn(squareIndex);
            
            let needDirections = [
                {row: -1, column: -1,},
                {row: +1, column: +1,},
                {row: +1, column: -1,},
                {row: -1, column: +1,},
                {row:  0, column: -1,},
                {row:  0, column: +1,},
                {row: +1, column:  0,},
                {row: -1, column:  0,},
            ];

            let enemySteps = [];
            let kingKillers = []; // фигуры, которые могут срубить короля
            if(!enemyChecked) {
                let currentSquaresTmp = Object.assign({}, currentSquares);
                currentSquaresTmp[squareIndex] = {
                    value: null,
                };
                for(let i = 0;i < this.boardSize*this.boardSize;i++) {
                    let enemyStepsInfo = this.getAvailableSteps(currentSquaresTmp, i, currentPlayer, false, true, true);
                    if(enemyStepsInfo && enemyStepsInfo.steps) {
                        // здесь сделать проверку, если наша, то  в один массив, если вражеская, то в enemySteps

                        // здесь нужно проверить, есть ли среди вражеских ходов, ход на текущую клетку
                        // если есть, то записать в kingKillers позицию текущей фигуры и все доступные ходы между текущей фигурой и королем
                        enemySteps = enemySteps.concat(enemyStepsInfo.steps);
                    }
                }
            }

            // здесь мы оставляем только те ходы наших фигур, которые могут срубить вражескую или сходить между вражеской и королем
            
            needDirections.forEach((offset) => {                
                let newRow = position.row + offset.row;
                let newColumn = position.column + offset.column;

                if(newRow > 0 && newRow <= this.boardSize && newColumn > 0 && newColumn <= this.boardSize) {
                    let checkIndex = this.getIndex(newRow, newColumn);
                    if(checkIndex >= 0 && checkIndex < this.boardSize*this.boardSize) {
                        if(currentSquares[checkIndex].value === null) {
                            if( enemySteps.indexOf(checkIndex) === -1 ) {
                                result.push(checkIndex);
                            }
                        } else if(currentSquares[checkIndex].value !== currentSquares[squareIndex].value) {
                            if( enemySteps.filter(index => index === checkIndex).length < 2 ) {
                                result.push(checkIndex);
                            }                          
                        }
                    }
                }                
            });

            return result;
        },
        getStepsPawn(currentSquares, squareIndex, enemySteps) {
            let result = [];
            let position = this.getRowColumn(squareIndex);
            let currentSquare = currentSquares[squareIndex];
            let currentPlayerId = currentSquare.value;

            let playerCond = {
                'white': {
                    firstStepRow: 7,
                    direction: -1,
                },
                'black': {
                    firstStepRow: 2,
                    direction: +1,
                },
            };

            let stepIndex = squareIndex + playerCond[currentPlayerId].direction*this.boardSize;

            if(position.row === playerCond[currentPlayerId].firstStepRow){
                let firstStepIndex = stepIndex + playerCond[currentPlayerId].direction*this.boardSize;
                if( currentSquares[stepIndex].value === null && currentSquares[firstStepIndex].value === null && !enemySteps ){
                    result.push(firstStepIndex);
                }                    
            }

            if( this.checkBoardSize(stepIndex) && currentSquares[stepIndex].value === null && !enemySteps ){
                result.push(stepIndex);
            }

            //chop steps                
            let chopIndexLeft = squareIndex + playerCond[currentPlayerId].direction*(this.boardSize + 1);
            let chopIndexRight = squareIndex + playerCond[currentPlayerId].direction*(this.boardSize - 1);
            let positionLeft = this.getRowColumn(chopIndexLeft);
            let positionRight = this.getRowColumn(chopIndexRight);

            if( positionLeft.row === (position.row + playerCond[currentPlayerId].direction) && this.checkBoardSize(chopIndexLeft) )
            {
                if( (currentSquares[chopIndexLeft].value !== null && currentSquares[chopIndexLeft].value !== currentPlayerId) || enemySteps) {
                    result.push( chopIndexLeft );
                }
            }
            if( positionRight.row === (position.row + playerCond[currentPlayerId].direction) && this.checkBoardSize(chopIndexRight) )
            {
                if( (currentSquares[chopIndexRight].value !== null && currentSquares[chopIndexRight].value !== currentPlayerId) || enemySteps) {
                    result.push( chopIndexRight );
                }
            }
            
            return result;
        },
        checkBoardSize(squareIndex){
            let result = true;
            let boardSize = this.boardSize;

            if(squareIndex < 0 || squareIndex > boardSize*boardSize ){
                result = false;
            }

            return result;
        },
        getChopIndex(indexFrom, indexTo, currentSquares){  
            let _this = this;

            let rowNumberFrom = Math.trunc(indexFrom / _this.boardSize);
            let columnNumberFrom = indexFrom % _this.boardSize;
            let rowNumberTo = Math.trunc(indexTo / _this.boardSize);
            let columnNumberTo = indexTo % _this.boardSize;
            let growRowIndex = rowNumberTo > rowNumberFrom ? 1 : -1;
            let growColumnIndex = columnNumberTo > columnNumberFrom ? 1 : -1;

            let playerId = currentSquares[indexFrom].value;
            let result = false;

            while(rowNumberFrom !== rowNumberTo) {
                rowNumberFrom += growRowIndex;
                columnNumberFrom += growColumnIndex;

                let indexCurrent = rowNumberFrom * _this.boardSize + columnNumberFrom;
                let squareCurrent = currentSquares[indexCurrent];
                if( squareCurrent.value && squareCurrent.value !== playerId ) {
                    result = indexCurrent;
                    break;
                }
            }

            return result;
            
        },
        checkDiagonal(currentIndex, currentSquares, element, onlyChop, currentPlayerId) {
            let _this = this;
            let rowNumber = Math.trunc(currentIndex / _this.boardSize);
            let columnNumber = currentIndex % _this.boardSize;
            let isKing = currentSquares[currentIndex].image === 'king';
            let maxDiag = isKing ? _this.boardSize : 2;
            let result = {
                chop: [],
                empty: [],
                result: false,
            };

            for(let diag = 1; diag < maxDiag; diag++){
                let index = (rowNumber + element.row*diag) * _this.boardSize + (columnNumber + element.column*diag);
                if(_this.checkBoardSize(rowNumber, columnNumber, element, _this.boardSize, diag)){
                    if(currentSquares[index].value === null){
                        if(result.chop.length) {
                            result.chop.push(index);
                        } else {
                            if( !onlyChop ) {
                                if(isKing || element.playerId === currentPlayerId) {
                                    result.empty.push(index);
                                    result.result = true;
                                }
                            }
                        }
                    } else if(currentSquares[index].value !== currentPlayerId) {
                        let indexChop = ( rowNumber + element.row*(diag + 1) ) * _this.boardSize + (columnNumber + element.column*(diag + 1));
                        if(_this.checkBoardSize(rowNumber, columnNumber, element, _this.boardSize, (diag + 1) )){
                            if(currentSquares[indexChop].value === null && !result.chop.length){
                                result.chop.push(indexChop);
                                result.result = true;
                            } else {
                                break;
                            }                    
                        }
                    } else {
                        break;
                    }       
                }
            }

            if(result.result) {
                return result;
            } else {
                return false;
            }
        },
        getRowColumn(index) {
            if( !this.checkBoardSize(index) )
                return false;

            let result = {
                row: Math.trunc(index / this.boardSize + 1),
                column: index % this.boardSize + 1,
            }
            return result;
        },
        getIndex(row, column) {
            if( (row > 0 && row <= this.boardSize) && (column > 0 && column <= this.boardSize) ) {
                return (row - 1) * this.boardSize + column - 1;
            }
            return false;
        }
    },
};

for (const key in GameMode) {
    GameMode[key].squares = GameMode[key].fillSquares( GameMode[key].boardSize );
}

export const GameModeContext = React.createContext(
    GameMode['tick-tack-toe'] // значение по умолчанию
);